# Vagrant Docker Provider

Experiments in replacing `docker compose` with Vagrant's Docker provider (note: not the provisioner)

## getting started.

```
$ vagrant up --provision
```

## known issues.

- [ ] ports are conflicting upon startup even though they're configured not to

## todo

- [ ] Private network, assigning each container its own physical host IP
- [ ] Figure-out env var forwarding and handling secrets
- [ ] Push button multi-arch support
- [ ] ???
